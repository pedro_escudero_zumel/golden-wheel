
var game;
var wheel;
var canSpin;
var prize;
var prizeText;
var moneyText;
var xhr = new XMLHttpRequest();
var xhr_money = new XMLHttpRequest();
var betResult = 0;
var degrees = 0;

window.onload = function() {
        game = new Phaser.Game(640, 500, Phaser.AUTO, 'game', { preload: preload, create: create, spin: spin, winPrize: winPrize });
        changeMoney();
}

var playGame = function(game){};

function preload(){
        game.load.image("wheel", "/trivial/wheels/base-wheel/images/wheel.png");
        game.load.image("wheel_exterior", "/trivial/wheels/base-wheel/images/wheel_exterior.png");
        game.load.image("pin", "/trivial/wheels/base-wheel/images/pin.png");
        game.load.image("background", "/trivial/wheels/base-wheel/images/background.png");
        game.load.image("total_bet", "/trivial/wheels/base-wheel/images/total_bet.png");
        game.load.image('header','../common/images/header.png');
        game.stage.backgroundColor = "#4488AA";
        canSpin = true;
}

function create(){
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.background = this.game.add.sprite(0,0, "background");
        header = this.game.add.sprite(this.game.world.centerX, 60, "header");
        header.scale.setTo(0.5,0.5);
        header.anchor.set(0.5);
        header.inputEnabled = true;
        header.input.pixelPerfectClick = true;
        header.input.pixelPerfectOver = true;
        wheel = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY + 30, 'wheel');
        wheel.scale.setTo(0.65,0.65);
        wheel.anchor.set(0.5);
        wheelExterior = game.add.sprite(this.game.world.centerX, 280, 'wheel_exterior');
        wheelExterior.scale.setTo(0.65,0.65);
        wheelExterior.anchor.set(0.5);
        pin = game.add.sprite(this.game.world.centerX, 280, "pin");
        pin.scale.setTo(0.45,0.45);
        pin.anchor.set(0.5);
        pin.inputEnabled = true;
        pin.input.useHandCursor = true
        this.background = this.game.add.sprite(20, 250, "total_bet");
        prizeText = game.add.text(60, 270, "", { font: "15px Arial", fill: "#fff", align: "center" });
        prizeText.anchor.set(0.2);
        prizeText.align = "left";
        prizeMoney = game.add.text(295, 50, "", { font: "15px Arial", fill: "#fff", align: "center" });
        prizeMoney.text = "";
        changeMoney();
        pin.events.onInputDown.add(fireServerCall, this);

        buy = game.add.text(375, 50, "", { font: "15px Arial", fill: "#fff", align: "center" });
        buy.inputEnabled = true;
        buy.text = "      ";
        buy.input.useHandCursor = true;
        buy.events.onInputDown.add(clickBuy, this);

        lobby = game.add.text(180, 50, "", { font: "15px Arial", fill: "#fff", align: "center" });
        lobby.inputEnabled = true;
        lobby.text = "         ";
        lobby.input.useHandCursor = true
        lobby.events.onInputDown.add(clickLobby, this);

        shareGame = game.add.text(420, 50, "", { font: "15px Arial", fill: "#fff", align: "center" });
        shareGame.inputEnabled = true;
        shareGame.text = "         ";
        shareGame.input.useHandCursor = true
        shareGame.events.onInputDown.add(clickShare, this);
}

function clickBuy(){
  $( "#popupLogin" ).popup( "open");
}

function clickLobby(){
  console.log('lobby');
  window.location.href = "../index.html";
}

function clickShare(){
  console.log('share');
  var url = window.location.href;
  FB.ui({
    method: 'share',
    href: url,
  }, function(response){});
}

function changeMoney(){
        xhr_money.open('POST', "../backend/balance.php", true);
        xhr_money.addEventListener("readystatechange", processMoney, true);
        xhr_money.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr_money.send('user_id=' + userId);
        return userId;
}

function processMoney(){
  if (xhr_money.readyState == 4 && xhr_money.status == 200) {
        data = JSON.parse(xhr_money.response);
        prizeMoney.text = data.balance;
  }
}

function fireServerCall(){
        xhr.open('POST', "../backend/bet.php", true);
        xhr.addEventListener("readystatechange", processRequest, true);
        xhr.addEventListener("error", handleError, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send('id=39403393&degrees=' + degrees + '&token=Z9df0a7dgS1ej1Em37grfa24o8RL&key=key&user='+ userId +'&project='+ projectId);
}

function handleError(error){
    alert("conection Error with the backend: " + error);
}

function processRequest() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          data = JSON.parse(xhr.response);
          if(data.status === 'false'){
              console.log(data.status);
              console.log(data);
              alert('Error conecting with server: Error '+ xhr.status);
              return;
          }
          betResult = data.bet;
          degrees = data.degrees;
          spin();
         }else if (xhr.status != 200 && xhr.readyState == 4){
             console.log("Error conection with the server ");
             alert('Error conecting with server: Error '+ xhr.status +' We will reload the page trying to resolve this issue.');
             window.location.reload(false);
         }
}

function spin(){
        if(canSpin){
            canSpin = false;
            prizeText.text = "";
            var rounds = game.rnd.between(4, 6);
            var spinTween = game.add.tween(wheel).to({
                angle: 360 * rounds + degrees
            }, 3000, Phaser.Easing.Quadratic.Out, true);
            spinTween.onComplete.add(this.winPrize, this);
        }
}

function winPrize(){
        canSpin = true;
        this.prizeText.text = betResult;
        changeMoney();
}
