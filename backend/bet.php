<?php
if($_POST['project']){
    $response = validate_call($_POST);
    if (!empty($response)){
        echo json_encode($response);
        exit;
    }
    $response = get_result($_POST['project']);
    $response['project'] = $_POST['project'];
    $transaction = array_merge($_POST, $response);
    $transaction['name'] = (int) $_POST['project'] > 2000 ? "Slot" : "Wheel";
    set_transaction($transaction);
    echo json_encode($response);
}elseif(!$_POST){
    $response['status'] = false;
    $response['error'] = "Critical error";
}

function validate_call($call){
    check_id($call['id']);
    check_token($call['token']);
    check_degrees($call['degrees']);
    check_project($call['project']);
}

function check_id($id){
    if($id != '39403393'){
        echo json_encode( array('status' => 'false', 'error1' => "true", 'id' => $id) );
        exit;
    }
}

function check_token($token){
    if($token != 'Z9df0a7dgS1ej1Em37grfa24o8RL'){
        echo json_encode( array('status' => 'false', 'error2' => "True") );
        exit;
    }
}

function check_degrees($degrees){
    if($degrees > 360 OR $degress < 0){
        echo json_encode( array('status' => 'false', 'error3' => "True") );
        exit;
    }
}

function check_project($project_id){
    if($project_id == 1001 || $project_id == 2001){
       return;
    }
    echo json_encode( array('status' => 'false', 'error4' => "True") );
    exit;
}

function calculate_prize($degrees){
    $slices = 8;
    $slicePrizes = ["100", "10", "500", "50", "150", "10", "200", "0"];
    $prize = ($slices - 1) - floor($degrees / (360 / $slices));
    return $slicePrizes[$prize];
}

function get_result($project_id){
    switch($project_id){
        case 1001:
            $response = get_wheel_response($project_id);
            set_balance($response['bet'], $_POST['user']);
            break;
        case 2001:
            $response = get_slot_response($project_id);
            set_balance($response['prize'], $_POST['user']);
            break;
    }
    return $response;
}

function get_wheel_response($project_id){
    $rand = rand(0, 360);
    $response = array('status' => 'true');
    $response['degrees'] = $rand;
    $response['id'] = $_POST['id'];
    $response['token'] = $_POST['id'];
    $response['bet'] = calculate_prize($rand);
    return $response;
}

function get_slot_response($project_id){
    $rand = rand(0, 99);
    $prizes = get_prizes();
    $response['reel1'] = $prizes[$rand][0];
    $response['reel2'] = $prizes[$rand][1];
    $response['reel3'] = $prizes[$rand][2];
    $response['reel4'] = $prizes[$rand][3];
    $response['reel5'] = $prizes[$rand][4];
    $response['status'] = true;
    $response['prize'] = $prizes[$rand][5];
    return $response;
}

function get_prizes(){
    $prizes = [
        0 => ['Life Mushroom', 'Life Mushroom', 'Life Mushroom', 'Life Mushroom', 'Life Mushroom', 1000 ],
        1 => ['Life Mushroom', 'Mario Hat', 'Life Mushroom', 'Life Mushroom', 'Fire Flower', 100 ],
        2 => ['Mario Hat', 'Life Mushroom', 'Fire Flower', 'Fire Flower', 'Life Mushroom', 0 ],
        3 => ['Mario Hat', 'Mario Hat', 'Life Mushroom', 'Life Mushroom', 'Fire Flower', 30 ],
        4 => ['Mario Hat', 'Goomba', 'Mario Hat', 'Life Mushroom', 'Goomba', 0 ],
        5 => ['Mario Hat', 'Goomba', 'Mario Hat', 'Life Mushroom', 'Goomba', 0 ],
    ];
    $prizes = array_merge($prizes, array_fill(6,40,['Mario Hat', 'Fire Flower', 'Life Mushroom', 'Goomba', 'Fire Flower', 0 ]));
    $prizes = array_merge($prizes, array_fill(47,20,['Mario Hat', 'Mario Hat', 'Fire Flower', 'Goomba', 'Fire Flower', 50 ]));
    $prizes = array_merge($prizes, array_fill(67,20,['Fire Flower', 'Fire Flower', 'Fire Flower', 'Goomba', 'Mario Hat', 80 ]));
    $prizes = array_merge($prizes, array_fill(87,15,['Goomba', 'Fire Flower', 'Goomba', 'Goomba', 'Goomba', 100 ]));
    return $prizes;
}
function set_balance($amount, $user_id){
    $sql = "UPDATE 00casino_bank set amount = amount + ". $amount ." where facebook_id = ".$user_id;
    require('db.php');
    $conn->query($sql);
}
function set_transaction($transaction){
    $description = (int) $transaction['project'] > 2000 ? "Slot" : "Wheel" ;
    $description = $transaction['bet'] ? $description."-".$transaction['bet'] : $description."-".$transaction['prize'];
    $sql = "INSERT INTO 00casino_transactions (ip, name, date, user_id, description) VALUES ('".$_SERVER['HTTP_REMOTE_IP']."','".$transaction['name']."','"
    .date("Y-m-d-h-i-s")."','"
    .$transaction['user']."','"
    .$description."')";
    require('db.php');
    $conn->query($sql);
}
?>
