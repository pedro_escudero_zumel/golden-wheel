# Contracts for Golden Wheels #

* All calls should use https
* We employ urlencoded in the body (POST)  calls

## Bet contract sugestion ##



### URL call ###
```  
<domain>/v1/bet
```
Temporal URL for testing:

https://saracarrion.com/trivial/backend/bet.php

### Body of call ###

id: user id (our user id, not facebook id).

degrees: degrees (Integer from 0 to 360).

token: token for the call.

key: aditional control string.

project: id of the game (integer).


Example:

```
id:39403393
degrees:300
token:Z9df0a7dgS1ej1Em37grfa24o8RL
key:key
project:1001
Content-Type:application/x-www-form-urlencoded
```
### Response ###

```
{"status":"true",
"degrees":357,
"id":"39403393",
"token":"39403393",
"bet":"10",
"project":"1001"}
```

status: True for response ok, false for errors.

degrees: Degrees that the wheel is going to move (0 to 360).

id: User id (our user id, not facebook id).

token: token for next call.

bet: Result of the bet.

project: Current project(game). 

### Curl example ###
```
curl -X POST \

  https://saracarrion.com/trivial/backend/bet.php \
  
  -H 'cache-control: no-cache' \
  
  -H 'content-type: application/x-www-form-urlencoded' \
  
  -H 'degrees: 300' \
  
  -H 'id: 39403393' \
  
  -H 'key: key' \
  
  -H 'postman-token: 5a2816dd-7705-74c9-0f70-f454af25efd7' \
  
  -H 'project: 1001' \
  
  -H 'token: Z9df0a7dgS1ej1Em37grfa24o8RL' \
  
  -d 'id=39403393&degrees=300&token=Z9df0a7dgS1ej1Em37grfa24o8RL&key=key&project=1001&Content-Type=application%2Fx-www-form-urlencoded'
  ```