# README #

Golden wheel project

## Base structure ##

/payments -> Payment files needed for facebook. It is just a xml file and a php needed for testing 

/backend -> Fake backend in php. It just answers the degrees and calculates the prize. It has some fake security methods (values for that are fixed).

/header -> Files and assets for common header. This has the following structure:
    
    /css/
    /images/
    /sounds/

/footer -> Files and assets for common footer.

/project_name_folder (like base-wheel folder) -> This has the following structure:

    /config.js
    /css/
    /images/
    /sounds/

game.js -> Code for the golden wheel. It is done using Phaser (javascript framework).

index.html -> Base file for the project. It is the lobby of the project.

facebook.js -> Code for handling facebook SDK.

phaser.min.js -> Gaming javascript framework.

## Create a new Golden Wheel ##

This project is designed to be able to create a new Golden Wheel in a very easy and direct way.

Steps:

1) Create a new project folder and add there the assets. All projects should have the same kind of assets (names, no the same files).

2) Create a config.js file in the root of the project. Add there the project name (same as project folder, this is used for path for all assets). We add here also the project_id used for connection later with the backend.

3) Create a html cloning the base structure from base-wheel.html file. There you need to change the config file loaded to the config file created in the step 2.

```
<script type="text/javascript" src="<project_name>/config.js"></script>
```